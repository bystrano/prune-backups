##
# prune-backups.awk -- a program to remove old backup files.
# =================
#
# Removes files according to a given set of rules based on their mtime.
#
# # Usage exemple:
#
# ```
# awk -f prune-backups.awk -v dir=backups/ -v rules=rm:1y%keep-monthly:3m%keep
# ```
#
# This will operate recursively on all the files in the "backups" directory. The
# files older than 1 year will be deleted. The files older than 3 months will be
# deleted too, but the first one of every month will be kept. Files newer than 3
# months will be left untouched.
#
# By default, this command won't touch your files, but simply print the files
# and the actions to be taken to stdout. Add the "-v commit=1" option to the
# command to actually delete the files.
#
# # Variables:
#  - **rules** : Specifies the actions to be executed. Must be a string of
#        *steps*, separated by "%"'s. Each *step* consists in an *action* string
#        followed by ":" and an *age_min* string.
#        *Actions* must be one of the strings "rm", "keep", "keep-yearly",
#        "keep-monthly", "keep-weekly" or "keep-daily".
#        The *age_min* string is either an integer or a integer followed by a
#        unit : "y", "m", "w" or "d", year, month, week or day. Default unit is
#        seconds.
#  - **commit** : No file will be deleted unless you set this variable to 1.
#  - **dir** : The directory in which we look for files. Uses the current
#        working directory if omitted.
#

function init() {

    # default values for variables
    if (length(rules) == 0)   rules   = "keep-monthly:90d"
    if (length(commit) == 0) commit = 0
    if (length(dir) == 0)    dir    = "."

    if (commit != 1) {
        print "Running in safe mode: no file will be deleted. Run with \"-v commit=1\" to perform the deletions." > "/dev/stderr"
        close("/dev/stderr")
    }

    if (length(from_file) == 0) {
        cmd = sprintf("find %s -type f -printf '%%TY-%%Tm-%%TW-%%Td %%p\n' | sort", dir);
    } else {
        # allow to read find's output from a file, for testing.
        cmd = sprintf("cat %s", from_file);
    }

    # allow to change today's date, for testing.
    if (length(ts_now) == 0) {
        "date +%s" | getline ts_now;
        close("date +%s")
    }
}

function parse_rules() {

    # regex tested in tests/rules-regex.sh
    action = "(keep(-(daily|weekly|monthly|yearly))?|rm)"
    max_age = "(:[0-9]+[dwmy]?)"
    rules_regex = sprintf("^(%s%s%)*(%s%s?)$", action, max_age, action, max_age)

    if ( match(rules, rules_regex) == 0 ) {
        printf "%s is not a valid rules string\n", rules
        exit 2;
    }

    split(rules, steps, "%")
    for (i in steps) {
        split(steps[i], s, ":")
        steps_action[i] = s[1]
        if (length(s[2]) == 0) {
            steps_age_min[i] = 0
        } else {
            steps_age_min[i] = age2sec(s[2])
        }
    }
}


BEGIN {
    init()
    parse_rules()

    i = 0
    next_step()

    FIELDWIDTHS="14 9999"

    while (( cmd | getline ) > 0) {

        if ($1 >= date_max) {
            next_step()
        }

        process_file()
    }
    close(cmd);
}


function next_step() {
    i++
    action = steps_action[i]
    date_max = ts2date(ts_now - steps_age_min[i])
    last = ""
}

function process_file() {

    switch (action) {
        case "rm":
            rm_file($2)
            break
        case "keep-yearly":
        case "keep-monthly":
        case "keep-weekly":
        case "keep-daily":
            update_current()

            if ( current != last ) {
                keep_file($2)
                last = current;
            } else {
                rm_file($2);
            }

            break
        case "keep":
        default:
            keep_file($2)
    }
}

function update_current() {

    switch (action) {
        case "keep-yearly":
            current = substr($1, 1, 4)
            break
        case "keep-monthly":
            current = substr($1, 1, 7)
            break
        case "keep-weekly":
            current = substr($1, 1, 10)
            break
        case "keep-daily":
            current = substr($1, 1, 13)
            break
    }
}


function keep_file(filename) {

    if (commit != 1) {
        print "keep " filename;
    }
}

function rm_file(filename) {

    print "rm " filename;
    if (commit == 1) {
        system(sprintf("rm \"%s\"", filename));
    }
}

function ts2date(ts) {

    date_cmd = "date --date=@%s +%%Y-%%m-%%W-%%d"

    sprintf(date_cmd, ts) | getline date
    close(sprintf(date_cmd, ts));

    return date
}

function age2sec(age) {

    age = gensub(/([0-9]+)d/, "\\1 * 60 * 60 * 24", "g", age)
    age = gensub(/([0-9]+)w/, "\\1 * 60 * 60 * 24 * 7", "g", age)
    age = gensub(/([0-9]+)m/, "\\1 * 60 * 60 * 24 * 30", "g", age)
    age = gensub(/([0-9]+)y/, "\\1 * 60 * 60 * 24 * 365", "g", age)

    calc_command = sprintf("awk 'BEGIN { print %s }'", age)

    calc_command | getline result
    close(calc_command)

    return result
}

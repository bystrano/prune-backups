#!/usr/bin/env bats

match_rules() {
    echo "$1" | awk '{\
action = "(keep(-(daily|weekly|monthly|yearly))?|rm)";\
max_age = "(:[0-9]+[dwmy]?)";\
regex = sprintf("^(%s%s%)*(%s%s?)$", action, max_age, action, max_age);\
print match($0, regex)\
}'
}

@test "validation regex" {
    [ $(match_rules "blabliblou") == 0 ]
    [ $(match_rules "keep") == 1 ]
    [ $(match_rules "rm") == 1 ]
    [ $(match_rules "keep-daily") == 1 ]
    [ $(match_rules "keep-weekly") == 1 ]
    [ $(match_rules "keep-monthly") == 1 ]
    [ $(match_rules "keep-yearly") == 1 ]

    [ $(match_rules "keep-yearly:3600") == 1 ]
    [ $(match_rules "rm:6m") == 1 ]

    [ $(match_rules "rm:1y%keep-monthly:3m%keep-weekly:1w") == 1 ]
    [ $(match_rules "rm:1y%keep-blabliblou:3m%keep-weekly:1w") == 0 ]

    [ $(match_rules "rm%keep-monthly:3m%keep-weekly:1w") == 0 ]
    [ $(match_rules "rm:1y%keep-monthly:3m%keep-weekly") == 1 ]
}

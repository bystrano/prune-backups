#!/usr/bin/env bats

# The tests are run the 16 november 2019
awk_cmd="awk -f prune-backups.awk -v ts_now=1573926108";

assert_equals() {

    diff --side-by-side --color=always <(echo "$1") <(echo "$2");
    [ "$1" == "$2" ]
}

@test "one step keep-monthly" {
    result="$($awk_cmd -v from_file=tests/data.txt -v rules=keep-monthly)"
    expected=$(cat << EOF
keep /backup-db/test-2019-03-14-11:27.sql.gz
rm /backup-db/test-2019-03-21-11:10.sql.gz
keep /backup-db/test-2019-04-17-16:54.sql.gz
rm /backup-db/test-2019-04-17-08:07.sql.gz
keep /backup-db/test-2019-06-05-11:10.sql.gz
rm /backup-db/test-2019-06-05-10:30.sql.gz
keep /backup-db/test-2019-07-02-10:07.sql.gz
rm /backup-db/test-2019-07-20-16:52.sql.gz
keep /backup-db/test-2019-08-03-16:31.sql.gz
rm /backup-db/test-2019-08-03-15:57.sql.gz
rm /backup-db/test-2019-08-17-14:45.sql.gz
rm /backup-db/test-2019-08-18-17:52.sql.gz
rm /backup-db/test-2019-08-19-19:38.sql.gz
rm /backup-db/test-2019-08-22-10:40.sql.gz
keep /backup-db/test-2019-09-05-19:09.sql.gz
rm /backup-db/test-2019-09-12-09:38.sql.gz
rm /backup-db/test-2019-09-14-17:09.sql.gz
rm /backup-db/test-2019-09-20-19:56.sql.gz
rm /backup-db/test-2019-09-20-20:02.sql.gz
rm /backup-db/test-2019-09-24-14:25.sql.gz
rm /backup-db/test-2019-09-28-12:07.sql.gz
keep /backup-db/test-2019-10-01-10:55.sql.gz
rm /backup-db/test-2019-10-16-11:12.sql.gz
rm /backup-db/test-2019-10-18-18:38.sql.gz
rm /backup-db/test-2019-10-25-14:37.sql.gz
rm /backup-db/test-2019-10-28-13:45.sql.gz
rm /backup-db/test-2019-10-31-09:18.sql.gz
keep /backup-db/test-2019-11-06-09:28.sql.gz
rm /backup-db/test-2019-11-06-11:49.sql.gz
rm /backup-db/test-2019-11-07-10:55.sql.gz
rm /backup-db/test-2019-11-07-23:16.sql.gz
rm /backup-db/test-2019-11-12-15:19.sql.gz
rm /backup-db/test-2019-11-15-11:45.sql.gz
rm /backup-db/test-2019-11-15-11:49.sql.gz
rm /backup-db/test-2019-11-15-12:21.sql.gz
EOF
)
    assert_equals "$result" "$expected";
}

@test "one step keep-monthly after 90 days" {
    result="$($awk_cmd -v from_file=tests/data.txt -v rules=keep-monthly:90d)"
    expected=$(cat << EOF
keep /backup-db/test-2019-03-14-11:27.sql.gz
rm /backup-db/test-2019-03-21-11:10.sql.gz
keep /backup-db/test-2019-04-17-16:54.sql.gz
rm /backup-db/test-2019-04-17-08:07.sql.gz
keep /backup-db/test-2019-06-05-11:10.sql.gz
rm /backup-db/test-2019-06-05-10:30.sql.gz
keep /backup-db/test-2019-07-02-10:07.sql.gz
rm /backup-db/test-2019-07-20-16:52.sql.gz
keep /backup-db/test-2019-08-03-16:31.sql.gz
rm /backup-db/test-2019-08-03-15:57.sql.gz
rm /backup-db/test-2019-08-17-14:45.sql.gz
keep /backup-db/test-2019-08-18-17:52.sql.gz
keep /backup-db/test-2019-08-19-19:38.sql.gz
keep /backup-db/test-2019-08-22-10:40.sql.gz
keep /backup-db/test-2019-09-05-19:09.sql.gz
keep /backup-db/test-2019-09-12-09:38.sql.gz
keep /backup-db/test-2019-09-14-17:09.sql.gz
keep /backup-db/test-2019-09-20-19:56.sql.gz
keep /backup-db/test-2019-09-20-20:02.sql.gz
keep /backup-db/test-2019-09-24-14:25.sql.gz
keep /backup-db/test-2019-09-28-12:07.sql.gz
keep /backup-db/test-2019-10-01-10:55.sql.gz
keep /backup-db/test-2019-10-16-11:12.sql.gz
keep /backup-db/test-2019-10-18-18:38.sql.gz
keep /backup-db/test-2019-10-25-14:37.sql.gz
keep /backup-db/test-2019-10-28-13:45.sql.gz
keep /backup-db/test-2019-10-31-09:18.sql.gz
keep /backup-db/test-2019-11-06-09:28.sql.gz
keep /backup-db/test-2019-11-06-11:49.sql.gz
keep /backup-db/test-2019-11-07-10:55.sql.gz
keep /backup-db/test-2019-11-07-23:16.sql.gz
keep /backup-db/test-2019-11-12-15:19.sql.gz
keep /backup-db/test-2019-11-15-11:45.sql.gz
keep /backup-db/test-2019-11-15-11:49.sql.gz
keep /backup-db/test-2019-11-15-12:21.sql.gz
EOF
)
    assert_equals "$result" "$expected";
}

@test "one step keep-yearly" {
    result="$($awk_cmd -v from_file=tests/data.txt -v rules=keep-yearly)"
    expected=$(cat << EOF
keep /backup-db/test-2019-03-14-11:27.sql.gz
rm /backup-db/test-2019-03-21-11:10.sql.gz
rm /backup-db/test-2019-04-17-16:54.sql.gz
rm /backup-db/test-2019-04-17-08:07.sql.gz
rm /backup-db/test-2019-06-05-11:10.sql.gz
rm /backup-db/test-2019-06-05-10:30.sql.gz
rm /backup-db/test-2019-07-02-10:07.sql.gz
rm /backup-db/test-2019-07-20-16:52.sql.gz
rm /backup-db/test-2019-08-03-16:31.sql.gz
rm /backup-db/test-2019-08-03-15:57.sql.gz
rm /backup-db/test-2019-08-17-14:45.sql.gz
rm /backup-db/test-2019-08-18-17:52.sql.gz
rm /backup-db/test-2019-08-19-19:38.sql.gz
rm /backup-db/test-2019-08-22-10:40.sql.gz
rm /backup-db/test-2019-09-05-19:09.sql.gz
rm /backup-db/test-2019-09-12-09:38.sql.gz
rm /backup-db/test-2019-09-14-17:09.sql.gz
rm /backup-db/test-2019-09-20-19:56.sql.gz
rm /backup-db/test-2019-09-20-20:02.sql.gz
rm /backup-db/test-2019-09-24-14:25.sql.gz
rm /backup-db/test-2019-09-28-12:07.sql.gz
rm /backup-db/test-2019-10-01-10:55.sql.gz
rm /backup-db/test-2019-10-16-11:12.sql.gz
rm /backup-db/test-2019-10-18-18:38.sql.gz
rm /backup-db/test-2019-10-25-14:37.sql.gz
rm /backup-db/test-2019-10-28-13:45.sql.gz
rm /backup-db/test-2019-10-31-09:18.sql.gz
rm /backup-db/test-2019-11-06-09:28.sql.gz
rm /backup-db/test-2019-11-06-11:49.sql.gz
rm /backup-db/test-2019-11-07-10:55.sql.gz
rm /backup-db/test-2019-11-07-23:16.sql.gz
rm /backup-db/test-2019-11-12-15:19.sql.gz
rm /backup-db/test-2019-11-15-11:45.sql.gz
rm /backup-db/test-2019-11-15-11:49.sql.gz
rm /backup-db/test-2019-11-15-12:21.sql.gz
EOF
)
    assert_equals "$result" "$expected";
}

@test "one step keep-weekly" {
    result="$($awk_cmd -v from_file=tests/data.txt -v rules=keep-weekly)"
    expected=$(cat << EOF
keep /backup-db/test-2019-03-14-11:27.sql.gz
keep /backup-db/test-2019-03-21-11:10.sql.gz
keep /backup-db/test-2019-04-17-16:54.sql.gz
rm /backup-db/test-2019-04-17-08:07.sql.gz
keep /backup-db/test-2019-06-05-11:10.sql.gz
rm /backup-db/test-2019-06-05-10:30.sql.gz
keep /backup-db/test-2019-07-02-10:07.sql.gz
keep /backup-db/test-2019-07-20-16:52.sql.gz
keep /backup-db/test-2019-08-03-16:31.sql.gz
rm /backup-db/test-2019-08-03-15:57.sql.gz
keep /backup-db/test-2019-08-17-14:45.sql.gz
rm /backup-db/test-2019-08-18-17:52.sql.gz
keep /backup-db/test-2019-08-19-19:38.sql.gz
rm /backup-db/test-2019-08-22-10:40.sql.gz
keep /backup-db/test-2019-09-05-19:09.sql.gz
keep /backup-db/test-2019-09-12-09:38.sql.gz
rm /backup-db/test-2019-09-14-17:09.sql.gz
keep /backup-db/test-2019-09-20-19:56.sql.gz
rm /backup-db/test-2019-09-20-20:02.sql.gz
keep /backup-db/test-2019-09-24-14:25.sql.gz
rm /backup-db/test-2019-09-28-12:07.sql.gz
keep /backup-db/test-2019-10-01-10:55.sql.gz
keep /backup-db/test-2019-10-16-11:12.sql.gz
rm /backup-db/test-2019-10-18-18:38.sql.gz
keep /backup-db/test-2019-10-25-14:37.sql.gz
keep /backup-db/test-2019-10-28-13:45.sql.gz
rm /backup-db/test-2019-10-31-09:18.sql.gz
keep /backup-db/test-2019-11-06-09:28.sql.gz
rm /backup-db/test-2019-11-06-11:49.sql.gz
rm /backup-db/test-2019-11-07-10:55.sql.gz
rm /backup-db/test-2019-11-07-23:16.sql.gz
keep /backup-db/test-2019-11-12-15:19.sql.gz
rm /backup-db/test-2019-11-15-11:45.sql.gz
rm /backup-db/test-2019-11-15-11:49.sql.gz
rm /backup-db/test-2019-11-15-12:21.sql.gz
EOF
)
    assert_equals "$result" "$expected";
}

@test "one step keep-daily" {
    result="$($awk_cmd -v from_file=tests/data.txt -v rules=keep-daily)"
    expected=$(cat << EOF
keep /backup-db/test-2019-03-14-11:27.sql.gz
keep /backup-db/test-2019-03-21-11:10.sql.gz
keep /backup-db/test-2019-04-17-16:54.sql.gz
rm /backup-db/test-2019-04-17-08:07.sql.gz
keep /backup-db/test-2019-06-05-11:10.sql.gz
rm /backup-db/test-2019-06-05-10:30.sql.gz
keep /backup-db/test-2019-07-02-10:07.sql.gz
keep /backup-db/test-2019-07-20-16:52.sql.gz
keep /backup-db/test-2019-08-03-16:31.sql.gz
rm /backup-db/test-2019-08-03-15:57.sql.gz
keep /backup-db/test-2019-08-17-14:45.sql.gz
keep /backup-db/test-2019-08-18-17:52.sql.gz
keep /backup-db/test-2019-08-19-19:38.sql.gz
keep /backup-db/test-2019-08-22-10:40.sql.gz
keep /backup-db/test-2019-09-05-19:09.sql.gz
keep /backup-db/test-2019-09-12-09:38.sql.gz
keep /backup-db/test-2019-09-14-17:09.sql.gz
keep /backup-db/test-2019-09-20-19:56.sql.gz
rm /backup-db/test-2019-09-20-20:02.sql.gz
keep /backup-db/test-2019-09-24-14:25.sql.gz
keep /backup-db/test-2019-09-28-12:07.sql.gz
keep /backup-db/test-2019-10-01-10:55.sql.gz
keep /backup-db/test-2019-10-16-11:12.sql.gz
keep /backup-db/test-2019-10-18-18:38.sql.gz
keep /backup-db/test-2019-10-25-14:37.sql.gz
keep /backup-db/test-2019-10-28-13:45.sql.gz
keep /backup-db/test-2019-10-31-09:18.sql.gz
keep /backup-db/test-2019-11-06-09:28.sql.gz
rm /backup-db/test-2019-11-06-11:49.sql.gz
keep /backup-db/test-2019-11-07-10:55.sql.gz
rm /backup-db/test-2019-11-07-23:16.sql.gz
keep /backup-db/test-2019-11-12-15:19.sql.gz
keep /backup-db/test-2019-11-15-11:45.sql.gz
rm /backup-db/test-2019-11-15-11:49.sql.gz
rm /backup-db/test-2019-11-15-12:21.sql.gz
EOF
)
    assert_equals "$result" "$expected";
}

@test "one step rm" {
    result="$($awk_cmd -v from_file=tests/data.txt -v rules=rm:90d)"
    expected=$(cat << EOF
rm /backup-db/test-2019-03-14-11:27.sql.gz
rm /backup-db/test-2019-03-21-11:10.sql.gz
rm /backup-db/test-2019-04-17-16:54.sql.gz
rm /backup-db/test-2019-04-17-08:07.sql.gz
rm /backup-db/test-2019-06-05-11:10.sql.gz
rm /backup-db/test-2019-06-05-10:30.sql.gz
rm /backup-db/test-2019-07-02-10:07.sql.gz
rm /backup-db/test-2019-07-20-16:52.sql.gz
rm /backup-db/test-2019-08-03-16:31.sql.gz
rm /backup-db/test-2019-08-03-15:57.sql.gz
rm /backup-db/test-2019-08-17-14:45.sql.gz
keep /backup-db/test-2019-08-18-17:52.sql.gz
keep /backup-db/test-2019-08-19-19:38.sql.gz
keep /backup-db/test-2019-08-22-10:40.sql.gz
keep /backup-db/test-2019-09-05-19:09.sql.gz
keep /backup-db/test-2019-09-12-09:38.sql.gz
keep /backup-db/test-2019-09-14-17:09.sql.gz
keep /backup-db/test-2019-09-20-19:56.sql.gz
keep /backup-db/test-2019-09-20-20:02.sql.gz
keep /backup-db/test-2019-09-24-14:25.sql.gz
keep /backup-db/test-2019-09-28-12:07.sql.gz
keep /backup-db/test-2019-10-01-10:55.sql.gz
keep /backup-db/test-2019-10-16-11:12.sql.gz
keep /backup-db/test-2019-10-18-18:38.sql.gz
keep /backup-db/test-2019-10-25-14:37.sql.gz
keep /backup-db/test-2019-10-28-13:45.sql.gz
keep /backup-db/test-2019-10-31-09:18.sql.gz
keep /backup-db/test-2019-11-06-09:28.sql.gz
keep /backup-db/test-2019-11-06-11:49.sql.gz
keep /backup-db/test-2019-11-07-10:55.sql.gz
keep /backup-db/test-2019-11-07-23:16.sql.gz
keep /backup-db/test-2019-11-12-15:19.sql.gz
keep /backup-db/test-2019-11-15-11:45.sql.gz
keep /backup-db/test-2019-11-15-11:49.sql.gz
keep /backup-db/test-2019-11-15-12:21.sql.gz
EOF
)
    assert_equals "$result" "$expected";
}

@test "multiple steps" {
    result="$($awk_cmd -v from_file=tests/data.txt -v rules=rm:90d%keep-weekly:10d)"
    expected=$(cat << EOF
rm /backup-db/test-2019-03-14-11:27.sql.gz
rm /backup-db/test-2019-03-21-11:10.sql.gz
rm /backup-db/test-2019-04-17-16:54.sql.gz
rm /backup-db/test-2019-04-17-08:07.sql.gz
rm /backup-db/test-2019-06-05-11:10.sql.gz
rm /backup-db/test-2019-06-05-10:30.sql.gz
rm /backup-db/test-2019-07-02-10:07.sql.gz
rm /backup-db/test-2019-07-20-16:52.sql.gz
rm /backup-db/test-2019-08-03-16:31.sql.gz
rm /backup-db/test-2019-08-03-15:57.sql.gz
rm /backup-db/test-2019-08-17-14:45.sql.gz
keep /backup-db/test-2019-08-18-17:52.sql.gz
keep /backup-db/test-2019-08-19-19:38.sql.gz
rm /backup-db/test-2019-08-22-10:40.sql.gz
keep /backup-db/test-2019-09-05-19:09.sql.gz
keep /backup-db/test-2019-09-12-09:38.sql.gz
rm /backup-db/test-2019-09-14-17:09.sql.gz
keep /backup-db/test-2019-09-20-19:56.sql.gz
rm /backup-db/test-2019-09-20-20:02.sql.gz
keep /backup-db/test-2019-09-24-14:25.sql.gz
rm /backup-db/test-2019-09-28-12:07.sql.gz
keep /backup-db/test-2019-10-01-10:55.sql.gz
keep /backup-db/test-2019-10-16-11:12.sql.gz
rm /backup-db/test-2019-10-18-18:38.sql.gz
keep /backup-db/test-2019-10-25-14:37.sql.gz
keep /backup-db/test-2019-10-28-13:45.sql.gz
rm /backup-db/test-2019-10-31-09:18.sql.gz
keep /backup-db/test-2019-11-06-09:28.sql.gz
keep /backup-db/test-2019-11-06-11:49.sql.gz
keep /backup-db/test-2019-11-07-10:55.sql.gz
keep /backup-db/test-2019-11-07-23:16.sql.gz
keep /backup-db/test-2019-11-12-15:19.sql.gz
keep /backup-db/test-2019-11-15-11:45.sql.gz
keep /backup-db/test-2019-11-15-11:49.sql.gz
keep /backup-db/test-2019-11-15-12:21.sql.gz
EOF
)
    assert_equals "$result" "$expected";
}

@test "transitions from one keep command to another" {
    result="$($awk_cmd -v from_file=tests/data2.txt -v rules=keep-monthly:1w%keep-weekly)";
    expected=$(cat << EOF
keep test1
keep test2
rm test3
EOF
)
    assert_equals "$result" "$expected"
}

@test "transitions from an rm command to keep command" {
    result="$($awk_cmd -v from_file=tests/data2.txt -v rules=rm:5d%keep-weekly)";
    expected=$(cat << EOF
rm test1
keep test2
rm test3
EOF
)
    assert_equals "$result" "$expected"
}

@test "transitions from an rm command to keep command 2" {
    result="$($awk_cmd -v from_file=tests/data2.txt -v rules=rm:3d%keep-weekly)";
    expected=$(cat << EOF
rm test1
rm test2
keep test3
EOF
)
    assert_equals "$result" "$expected"
}

.PHONY: test    # run the test suite
.PHONY: lint    # run gawk's linter
.PHONY: doc     # build the docs from the source

SRC := prune-backups.awk

BATS      := vendor/bats/bin/bats
BATS_REPO := https://github.com/bats-core/bats-core.git

vendor:
	mkdir -p $@

$(BATS): | vendor
	git clone $(BATS_REPO) vendor/bats

test: | $(BATS)
	$(BATS) tests/*


lint:
	@awk --lint=invalid -f $(SRC) -v from_file=tests/data-lint.txt 2>&1


README.md: $(SRC)
	awk '/^#/, /^#/ { gsub(/^#+ ?/, "", $$0); print }' $(SRC) > $@

doc: README.md

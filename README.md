
prune-backups.awk -- a program to remove old backup files.
=================

Removes files according to a given set of rules based on their mtime.

# Usage exemple:

```
awk -f prune-backups.awk -v dir=backups/ -v rules=rm:1y%keep-monthly:3m%keep
```

This will operate recursively on all the files in the "backups" directory. The
files older than 1 year will be deleted. The files older than 3 months will be
deleted too, but the first one of every month will be kept. Files newer than 3
months will be left untouched.

By default, this command won't touch your files, but simply print the files
and the actions to be taken to stdout. Add the "-v commit=1" option to the
command to actually delete the files.

# Variables:
 - **rules** : Specifies the actions to be executed. Must be a string of
       *steps*, separated by "%"'s. Each *step* consists in an *action* string
       followed by ":" and an *age_min* string.
       *Actions* must be one of the strings "rm", "keep", "keep-yearly",
       "keep-monthly", "keep-weekly" or "keep-daily".
       The *age_min* string is either an integer or a integer followed by a
       unit : "y", "m", "w" or "d", year, month, week or day. Default unit is
       seconds.
 - **commit** : No file will be deleted unless you set this variable to 1.
 - **dir** : The directory in which we look for files. Uses the current
       working directory if omitted.

